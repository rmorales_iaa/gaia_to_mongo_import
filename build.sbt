name := "gaia_to_mongo_import"

version := "1.0"

scalaVersion := "2.11.8"

lazy val mainClassName = "Main"

mainClass in (Compile, run) := Some(mainClassName)

// mongoDB. https://github.com/mongodb/casbah/
libraryDependencies += "org.mongodb" % "casbah-core_2.11" % "3.1.1"
libraryDependencies += "org.mongodb" % "casbah-commons_2.11" % "3.1.1"

// utility : CSV
libraryDependencies += "com.github.tototoshi" %% "scala-csv" % "1.3.5"

// utility : Logging
libraryDependencies += "org.slf4j" % "slf4j-simple" % "1.7.25"
libraryDependencies += "ch.qos.logback" % "logback-classic" % "1.2.3"

// utility : IO
libraryDependencies += "com.github.scala-incubator.io" %% "scala-io-file" % "0.4.3-1"


//=============================================================================

//merge strategy
assemblyMergeStrategy in assembly  := {
    
    case PathList("META-INF", xs @ _*) => MergeStrategy.discard
    case "about.html" => MergeStrategy.rename    
    case _ => MergeStrategy.last
}
//=============================================================================
