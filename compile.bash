#!/bin/bash

OUTPUT_PATH="target/scala-2.11/"
USER_PATH="deploy/"

#sbt compile assembly eclipse
sbt compile eclipse assembly

echo "Moving created jar file to: "$USER_PATH
mv  $OUTPUT_PATH*.jar $USER_PATH
ls -la $USER_PATH

#end of file
