//------------------------------------------------------------------
package gaia
//--------------------------------------------------------------------
object ColDefinition {
  
  //------------------------------------------------------------------
  //gaia table definition
  //GAIA" col name, type, GAIA2 col pos (first col has index 0)
  val gaiaColBaseDefinitionList =  List(
      ("_id", 	                    "long",	2)  //'source_id', but it changed to match MongoDB object if name
      ,  ("ref_epoch", 	                  "double",	4)
      ,  ("ra", 	                        "double",	5)
      ,  ("ra_error", 	                  "float",	6)
      ,  ("dec",                     	    "double",	7)
      ,  ("dec_error",	                  "float",	8)
      ,  ("parallax", 	                  "double",	9)
      ,  ("parallax_error",               "float",	10)
      ,  ("parallax_over_error",	        "float",	11)
      ,  ("pm", 	                        "float",	12)
      ,  ("pmra", 	                      "double",	13)
      ,  ("pmra_error", 	                "float",	14)
      ,  ("pmdec", 	                      "double",	15)
      ,  ("pmdec_error", 	                "float",	16)
      ,  ("ruwe",                         "float",	55)
      ,  ("duplicated_source", 	          "boolean",64)
      ,  ("phot_g_n_obs", 	              "short",	65)
      ,  ("phot_g_mean_flux", 	          "double",	66)
      ,  ("phot_g_mean_flux_error", 	    "float",	67)
      ,  ("phot_g_mean_flux_over_error",  "float",	68)
      ,  ("phot_g_mean_mag", 	            "float",	69)
      ,  ("phot_bp_n_obs", 	              "short",	70)
      ,  ("phot_bp_mean_flux", 	          "double",	71)
      ,  ("phot_bp_mean_flux_error", 	    "float",	72)
      ,  ("phot_bp_mean_flux_over_error", "float",	73)
      ,  ("phot_bp_mean_mag", 	          "float",	74)
      ,  ("phot_rp_n_obs", 	              "short",	75)
      ,  ("phot_rp_mean_flux", 	          "double",	76)
      ,  ("phot_rp_mean_flux_error", 	    "float",	77)
      ,  ("phot_rp_mean_flux_over_error", "float",	78)
      ,  ("phot_rp_mean_mag", 	          "float",	79)
      ,  ("phot_proc_mode",               "byte",	  84)
      ,  ("phot_bp_rp_excess_factor", 	  "float",	85)
      ,  ("bp_rp", 	                      "float",	86)
      ,  ("bp_g",                         "float",	87)
      ,  ("g_rp", 	                      "float",	88)
      ,  ("radial_velocity", 	            "float",	89)
      ,  ("radial_velocity_error",	      "float",	90)
      ,  ("rv_nb_transits",               "short",	91)
      ,  ("L",	                          "double",	95)
      ,  ("B",	                          "double",	96)
      ,  ("ecl_lon", 	                    "double",	97)
      ,  ("ecl_lat", 	                    "double",	98)
  )						
      
  val gaiaBaseColPosList = gaiaColBaseDefinitionList.map(_._3)
    
  //------------------------------------------------------------------
  
  val wolfRayetColDefinitionList =  List(
       ("_id",                          "long"   ,0)  	
      ,  ("WR#",                        "string" ,1)	
      ,  ("Reference_1",                "string" ,2)	
      ,  ("Name",                       "string" ,3)	
      ,  ("Name2",                      "string" ,4)	
      ,  ("Name3",                      "string" ,5)	
      ,  ("ra",                         "double" ,6)  //J2000	
      ,  ("dec",                        "double" ,7)	//J2000
      ,  ("Galactic Longitude",         "double" ,8)	
      ,  ("Galactic Latitude",          "double" ,9)	
      ,  ("Spectral Type",              "string" ,10)	
      ,  ("Reference_2",                "string" ,11)	
      ,  ("Binary Status",              "string" ,12)	
      ,  ("Binary Reference",           "string" ,13)	
      ,  ("u",                          "string" ,14)	
      ,  ("b",                          "string" ,15)	
      ,  ("v",                          "string" ,16)	
      ,  ("r",                          "string" ,17)	
      ,  ("U",                          "string" ,18)	
      ,  ("B",                          "string" ,19)	
      ,  ("V",                          "string" ,20)	
      ,  ("R",                          "string" ,21)	
      ,  ("I",                          "string" ,21)	
      ,  ("J",                          "string" ,23)	
      ,  ("H",                          "string" ,24)	
      ,  ("K",                          "string" ,25)	
      ,  ("Cluster",                    "string" ,26)	
      ,  ("Association",                "string" ,27)	
      ,  ("Cluster Distance",           "string" ,28)	
      ,  ("Reference",                  "string" ,29)	
      ,  ("Photometric Distance",       "string" ,30)
      ,  ("gaia2_ra_dec_box_dimension", "double" ,31))
      
   
 val wolfRayetColPosList = wolfRayetColDefinitionList.map(_._3)
   
 //------------------------------------------------------------------
  
 val geometricDistanceColDefinitionList =  List(
       ("_id",                         "long"   ,0)  	 //original name is 'source_id', but it changed to match MongoDB object if name
      ,  ("r_est",                     "double" ,1)	
      ,  ("r_lp",                      "double" ,2)	
      ,  ("r_hi",                      "double" ,3)	
      ,  ("r_len",                     "double" ,4)	
      ,  ("result_flag",               "int"    ,5)
      ,  ("modality_flag",             "int"    ,6))
 val geometriDistanceColPosList = geometricDistanceColDefinitionList.map(_._3)
 
//------------------------------------------------------------------
/*
//Add new columns  
use GAIA_DR_2
white-space: pre-line
db.gaia_source.update({},{ $set : {
  "phot_rp_mean_flux_over_error":0 
  ,  "phot_rp_mean_mag":0
  ,  "phot_bp_rp_excess_factor":0
  ,  "phot_proc_mode":0
  ,  "bp_rp":0
  ,  "bp_g":0 
  ,  "g_rp":0 
  ,  "radial_velocity":0
  ,  "radial_velocity_error":0
  ,  "rv_nb_transits":0
  } }
  ,{ 
   upsert:true,
   multi:true
}) 
db.gaia_source.findOne()
*/
//------------------------------------------------------------------
 
//------------------------------------------------------------------
//------------------------------------------------------------------
}