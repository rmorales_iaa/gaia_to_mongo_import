//--------------------------------------------------------------------
//This program inserts into mongoDB the Gaia database 
//http://mongodb.github.io/casbah/3.1/getting-started/
//http://api.mongodb.com/scala/casbah/2.5.0/tutorial.html
//--------------------------------------------------------------------
package gaia
//--------------------------------------------------------------------
import java.io.File
import java.nio.file.Paths
//--------------------------------------------------------------------
import org.slf4j.{Logger, LoggerFactory}
import java.util.concurrent.atomic.AtomicLong
import scala.collection.mutable.ArrayBuffer
//--------------------------------------------------------------------
import ColDefinition._
import com.mongodb.casbah.{MongoClient, MongoCollection}
import com.mongodb.casbah.Imports._
import gaia.parallelTask.ParallelTask
//--------------------------------------------------------------------
object Main extends App {
  
  //------------------------------------------------------------------
  private var Log : Logger = null
  private var FileSeparator = "" 
  
  //mongoDB
  private var mongoClient: MongoClient = null
  private var mongoCollection: MongoCollection = null
  private var mongoCollectionGaia: MongoCollection = null
  
  //user input
  private var rootInputDir = ""
  private var coreCount = 1
  private var uri = ""
  private var databaseName = ""
  private var collectionName = "" 
  private var dropCollection = false
  
  private var colDef: List[(String, String, Int)] = null
  private var colPos: List[Int] = null
  
  private var isParsingGaia =               true
  private var isParsingWolfRayet =          false
  private var isParsingGeometricDistance =  false
  
  private var fileCountImported: AtomicLong = null 
  private var itemCountParsed: AtomicLong = null
  
  private final val WOLF_RAYET_RANGE     =  0.0001
  private final val WOLF_RAYET_RANGE_STEP = 0.0001
  
  //Wolt-rayet set of source_id inserted
  var wolfRayetSourceIDSet : scala.collection.mutable.SortedSet[String] = null
  
  //stat
  private var wolfRayetMultipleSourceCount: Long = 0
  private var wolfRayetDuplicatedSourceCount: Long = 0
  
  private var geometricDistanceError: Long = 0  
  
  //log
  var loggerFile: MyFile = null
  //-------------------------------------------------------------------------
  def info(s: String) = {
    Log.info(s)
    val t = MyFile.getISO_DateLocalTimeStampWindowsCompatible()
    loggerFile.writeLine(t+"::"+s)
  }
  //-------------------------------------------------------------------------
  def error(s: String) = {
    Log.error("Error->"+s)
    val t = MyFile.getISO_DateLocalTimeStampWindowsCompatible()
    loggerFile.writeLine(t+"::"+s)
  }
  //-------------------------------------------------------------------------
  def warning(s: String) = {
    Log.warn("Warning->"+s)
    val t = MyFile.getISO_DateLocalTimeStampWindowsCompatible()
    loggerFile.writeLine(t+"::"+s)
  }
  //-------------------------------------------------------------------------
  // code section 
  //-------------------------------------------------------------------------
  import java.util.concurrent.TimeUnit
  private def getString(milliSeconds:Long) : String ={
    var m = milliSeconds
    val days = TimeUnit.MILLISECONDS.toDays(m)
    m -= TimeUnit.DAYS.toMillis(days);   
    val hours = TimeUnit.MILLISECONDS.toHours(m)
    m -= TimeUnit.HOURS.toMillis(hours)
    val minutes = TimeUnit.MILLISECONDS.toMinutes(m)
    m -= TimeUnit.MINUTES.toMillis(minutes)
    val seconds = TimeUnit.MILLISECONDS.toSeconds(m)
    days+"D "+hours+"H "+minutes+"m "+seconds+"s"
  }  
  //-------------------------------------------------------------------------
  private def directoryExist(s:String) : Boolean ={ 
    val f = new File(s)
    f.exists && f.isDirectory
  }   
  //------------------------------------------------------------------------- 
  private def sortFileByAbsolutePath (f1: File, f2: File) : Boolean = 
    f1.getAbsolutePath < f2.getAbsolutePath
    
  //-------------------------------------------------------------------------  
  private def getSortFileListWithExtension(dir: String, extension: String): List[File] = {    
    if (!directoryExist(dir)) return List[File]()    
    new File(dir).listFiles.filter(_.isFile).toList.filter { file =>
        file.getName.endsWith(extension) }.sortWith(sortFileByAbsolutePath(_,_))    
  }
  //-------------------------------------------------------------------------  
  private def ensureEndWithFileSeparator(s:String) : String = if (s.endsWith(FileSeparator)) s else s+FileSeparator    
  //-------------------------------------------------------------------------  
  private def getPathOnlyFilename(s: String) : String = Paths.get(s).getFileName.toString
  //-------------------------------------------------------------------------  
  private def getPathParent(s: String) : String = Paths.get(s).getParent.toString+FileSeparator
  //-------------------------------------------------------------------------
  private def removeFileExtension(s: String) = s.slice(s.lastIndexOf("/")+1,s.lastIndexOf("."))
  //---------------------------------------------------------------------------
  def getPartitionInt(min: Int, max: Int, pSize: Int) : Array[Array[Int]] = { //(min,max) included
    val range = max - min + 1
    if (range <= pSize) (for(i<-min to max) yield Array(i)).toArray
    else{
      val completePartitionCount = range  / pSize
      val remainPartition = range  % pSize
      val r = for(i<-0 until pSize) yield
        ArrayBuffer.tabulate(completePartitionCount)(n=> min + (i *completePartitionCount) + n)
      val offset = r.last.last + 1
      for(i<-0 until remainPartition)
        r(i) += (i + offset)
      (r map (_.toArray)).toArray
    }
  }
  //------------------------------------------------------------------
  private def getZeroWhenEmpty(t:(String, Int)) : String = if (t._1.isEmpty) return "0"  else t._1
  //------------------------------------------------------------------
  private def buildProjectionCol(l: List[String]) = {
    
    var p = MongoDBObject.empty
    l.foreach { x => p ++= MongoDBObject( x -> 1) }
    p
  }
  //------------------------------------------------------------------
  private def parserLine(line: List[String], colPos: List[Int]) : List[String] =     
    line.zipWithIndex.filter(pair => colPos
        .contains(pair._2)) //get only the required col   
        .map(getZeroWhenEmpty(_))        
  //------------------------------------------------------------------
  private def update(builder: BulkWriteOperation, line: List[String], id: Long) = {
    
    val query = MongoDBObject("_id" -> id)
    if (mongoCollection.find(query).limit(1).count() > 0) {                
      
      val l = parserLine(line, ColDefinition.gaiaBaseColPosList)  
      val updateVal = buildMongoObject( ColDefinition.gaiaColBaseDefinitionList, l, true )
      
      ///builder.find(query).updateOne(updateVal)
      builder.find(query).updateOne($set("rv_nb_transits" -> 34))
    }
    else {
       error(s"Can not find the object: '" + builder)
       System.exit(-1)
    }   
  }   
  
  //------------------------------------------------------------------
  private def parseLineWolfRayet(line: List[String]) : List[String] = {
       
    if (wolfRayetSourceIDSet == null) wolfRayetSourceIDSet = scala.collection.mutable.SortedSet[String]()
    val raSource =  str2raJ2000(line(6))
    val decSource = str2decJ2000(line(7))            
    val r = line.zipWithIndex.map{ case(e,i) => 
       if(i == 6) raSource.toString
       else if(i == 7) decSource.toString 
       else e}
    
    //find the corresponding GAIA source and add it as first value
    val gaiaMatchListResult = getGaiaSourceWithStep(raSource, decSource, WOLF_RAYET_RANGE, WOLF_RAYET_RANGE_STEP)    
    val gaiaMatchList = gaiaMatchListResult._1
    val boxDimension = gaiaMatchListResult._2
    val copyMatchList = gaiaMatchList.copy
    val closestSource = getClosestSource(gaiaMatchList, raSource, decSource)
    if (copyMatchList.count > 1){
      wolfRayetMultipleSourceCount += 1
      warning(s"Wolf rayet '${line(0)}' at ($raSource,$decSource) multiple sources. Match count: ${copyMatchList.count}")
      copyMatchList.foreach( t=> info("\t"+ t.toString))                                 
    }
    
    var newID = closestSource._1
    if (wolfRayetSourceIDSet.contains(newID)) {
       wolfRayetDuplicatedSourceCount += 1
       error(s"Wolf rayet '${line(0)}' at ($raSource,$decSource) . Duplicated GAIA source: $newID")          
       newID = "-" + line(0)
    }
    else wolfRayetSourceIDSet += newID      
        
    val result = r.zipWithIndex.map{case (t,i) => i match {
        case 0 => newID
        case 6 => closestSource._2
        case 7 => closestSource._3
        case anyOther => t
      } 
    }
    
    //too much log
    info (s"Selected source in GAIA for Wolf-Rayet ${line(0)} -> ${closestSource._1}")
    result ::: List(boxDimension.toString)    
  }
  
  //------------------------------------------------------------------
  private def parseLineGeometricDistance(line: List[String]) : List[String] = {
    
    val r = parserLine(line, colPos)
    val id = r(0).toLong
    if (!existGaiaSourceID(id)){
       geometricDistanceError += 1
       error(s"Geometric distance source with souce_id: $id was not found in GAIA")
       r.zipWithIndex.map{case (t,i) => if (i == 0) "-"+id else t}
    }
    else r
  }
  //------------------------------------------------------------------
  private def existGaiaSourceID(id: Long) : Boolean = {
    val q: DBObject = ("_id"  $eq id )
    mongoCollectionGaia.findOne( q ).size == 1
  }
  //------------------------------------------------------------------
  import com.github.tototoshi.csv.CSVReader  
  private def parseFile(fileName: String ) : Boolean = {

    val reader =  CSVReader.open(fileName)
    info(s"Importing sources from file $fileName")     
    var firstLine = true
            
    try {
      reader.readNext()  //avoid header
      var line = reader.readNext()
      val builder = mongoCollection.initializeOrderedBulkOperation
            
      //add all lines to mongoObject
      while(line != None) {       
        if (line.isDefined) {
          
          val valueList = if (isParsingWolfRayet) parseLineWolfRayet(line.get)          
                          else 
                            if (isParsingGeometricDistance) parseLineGeometricDistance(line.get)
                            else parserLine(line.get, colPos)   
                          
          if (firstLine) {
            firstLine = false                        
            fileCountImported.addAndGet(1)
            if (!dropCollection && isParsingGaia){ //parse not processing files              
              if (existGaiaSourceID(valueList(0).toLong)) return false                      
            }
          }       

          //if(!dropCollection) update(builder, line.get, l(0).toLong)          
          builder.insert( buildMongoObject( colDef, valueList ) )
        }
        else error(s"Error importing line: '$line'")         
        line = reader.readNext()
      }      
      //insert in batch mode
      val r = builder.execute
      if (r.isAcknowledged) {
        itemCountParsed.addAndGet(r.getInsertedCount)
        info("Imported " + r.getInsertedCount + s" sources from file $fileName")       
      }
      else error(s"Error importing file: '$fileName'")
    } 
    catch {
      case e:Exception => error(e.toString)
    } 
    finally {
      reader.close
    }
    true
  }
  
  //---------------------------------------------------------------------------
  private def process() = {
    //-------------------------------------------------------------------------
    val storage =  ArrayBuffer[String]()
    val l = getSortFileListWithExtension(rootInputDir,".csv")
    warning(s"Parsing ${l.size} files from '$rootInputDir'")
    getSortFileListWithExtension(rootInputDir,".csv").foreach { f => storage += f.getAbsolutePath}
    val partitionSeq = getPartitionInt(0,storage.length-1,coreCount)
    //-------------------------------------------------------------------------
    class MyParallelTask(indexSeq: Array[Array[Int]]) extends ParallelTask[Array[Int]] (
      indexSeq
      , coreCount
      , isItemProcessingThreadSafe = true
      , randomStartMaxMsWait = 10){
      //-----------------------------------------------------------------------
      def userProcessSingleItem(posSeq: Array[Int]) : Unit = {
        for(i <- posSeq)
          parseFile(storage(i))
      }
      //-----------------------------------------------------------------------
    }
    new MyParallelTask(partitionSeq)
    //-------------------------------------------------------------------------
  }
  //------------------------------------------------------------------
  private def connectWithMongoDB (uriString: String, databaseName: String, collectionName: String) {
   
    import com.mongodb.casbah.Imports._
    import com.mongodb.casbah.MongoClientURI
    import com.mongodb.casbah.MongoClient
    
    info("Connecting with Mongo database")
    mongoClient = MongoClient( MongoClientURI( uriString ) )    
    mongoCollection = mongoClient (databaseName)(collectionName)    
    
    mongoCollectionGaia = mongoClient (databaseName)("gaia_source") //fix connection
  }

  //------------------------------------------------------------------
  private def disconnectMongoClient   {
    
    info("Closing MongoClient")
    mongoClient.close()
  }

  //------------------------------------------------------------------
  private def buildMongoObject( colDefinition: List[(String,String,Int)]
  , line: List [String]
  , update: Boolean = false) : MongoDBObject = {
  
     val mongoObject = new MongoDBObject
     
    //build the row to be inserted in MongoDB
    (line zip colDefinition).map { case (v, col) =>
       val value = col._2 match {
         case "boolean"=> v.toBoolean
         case "byte"   => v.toByte
         case "short"  => v.toShort
         case "int"    => v.toInt
         case "long"   => v.toLong
         case "float"  => v.toFloat
         case "double" => v.toDouble
         case "string" => v
       }               
       if (update) mongoObject ++= MongoDBObject("$set" -> MongoDBObject(col._1 -> value))
       else        mongoObject ++= MongoDBObject(col._1 -> value)
     }        
     mongoObject
   }
 //------------------------------------------------------------------
 //Always generate a result
   private def getGaiaSourceWithStep(ra: Double, dec: Double, range: Double, rangeStep: Double) : (MongoCursor, Double) = {
    
     var r = getGaiaSource(ra, dec, range)
     if (!r.isEmpty) (r, range)
     else getGaiaSourceWithStep(ra, dec, range+rangeStep, rangeStep)
   }
  //------------------------------------------------------------------
  private def getGaiaSource(ra: Double, dec: Double, range: Double) = {
    
    val q: DBObject = ("ra"  $gte ra-range   $lte ra+range) ++ 
                      ("dec" $gte dec-range  $lte dec+range)
    mongoCollectionGaia.find( q )    
  }

  //------------------------------------------------------------------
  //closest point using geometric distance (ra,dec)
  private def getClosestSource(r: MongoCursor, ra_0: Double, dec_0: Double) = {
    
    var min: Double = Double.MaxValue
    var minId: String = ""
    var minRa: String = ""
    var minDec: String = ""    
    
    r.foreach { t=> val raGaia =  t.get("ra").asInstanceOf[Double]
                    val decGaia = t.get("dec").asInstanceOf[Double]
                    val idGaia =  t.get("_id").asInstanceOf[Long]
    
      val d = Math.sqrt(Math.pow((ra_0 - raGaia),2) + Math.pow((dec_0 - decGaia),2))       
      if (d < min){                            
        min = d
        minId = idGaia.toString
        minRa = raGaia.toString
        minDec = decGaia.toString
      }
    }    
    (minId,minRa,minDec)
  }
  
  //------------------------------------------------------------------
  private def parseUserConfiguration(argList: Array[String])  {
 
    if ( argList.size != 6 ) {
      println("Error. Incorrect number of parameters. Provided: "+argList.size)
      println("User input:" + argList.mkString(","))
      println("Import a set of CSV files to MongoDB. Only a specific columns will be translated.")
      println("Syntax:  java -jar gaia_to_cass_import-assembly-1.0.jar rootInputDir coreCount uri databaseName collectionName dropCollection")
      println("Example: java -jar gaia_to_cass_import-assembly-1.0.jar input 2 mongodb://user:pass@localhost:27017 GAIA_DR_2 gaia_source 1")      
      println("Example: java -jar gaia_to_cass_import-assembly-1.0.jar input 1 mongodb://user:pass@localhost:27017 GAIA_DR_2 wolf_rayet 1")
      println("Example: java -jar gaia_to_cass_import-assembly-1.0.jar input 1 mongodb://user:pass@localhost:27017 GAIA_DR_2 geometric_distance 1")
      System.exit(1)
    }
        
    rootInputDir   = argList(0)
    coreCount    = argList(1).toInt
    uri            = argList(2)
    databaseName   = argList(3)
    collectionName = argList(4)
    dropCollection = argList(5) == "1"
        
    collectionName match {
      case "gaia_source" =>
        isParsingGaia = true
        isParsingWolfRayet = false
        isParsingGeometricDistance =  false
        
        colDef = gaiaColBaseDefinitionList
        colPos = gaiaBaseColPosList
        info("Importing 'gaia2' tables")
        
      case "wolf_rayet" =>
        isParsingGaia  = false
        isParsingWolfRayet = true
        isParsingGeometricDistance =  false
        
        colDef = wolfRayetColDefinitionList
        colPos = wolfRayetColPosList
        info("Importing 'Wolf-Rayet' table")
        
      case "geometric_distance" =>
        isParsingGaia  = false
        isParsingWolfRayet = false
        isParsingGeometricDistance = true
        
        colDef = geometricDistanceColDefinitionList
        colPos = geometriDistanceColPosList
        info("Importing 'geometric_distance' table")
        
       
      case t => error("Unknown collection name")
                System.exit(1)        
    }
            
    info("Input CVS dir:  " + rootInputDir)
    info("Thread count:   " + coreCount)
    info("Uri:            " + uri)
    info("DatabaseName:   " + databaseName)
    info("CollectionName: " + collectionName)
    info("Drop collection:" + dropCollection)    
  }
  
  //------------------------------------------------------------------
  //Return the declination in degrees from sexagesimal or decimal degrees 
  //adapted from libwcs/hget.c
  
  def str2raJ2000 (in: String) : Double = str2decJ2000(in) * 15
  
  //------------------------------------------------------------------  
  def str2decJ2000 (in: String) : Double = {

    var dec: Double = 0		/* Declination in degrees (returned) */
    
    if ((in == null) || (in.isEmpty())) return dec
    
    val seq = in.trim.replace(":", " ").trim.split(" ")    
    val deg = seq(0).toDouble
    val min = seq(1).toDouble
    val sec = if (seq.size == 2) 0.0 else seq(2).toDouble
    	     
	  dec = Math.abs(deg) + (min / 60.0) + (sec / 3600.0)
	     
	  if (deg < 0) -dec 
	  else dec
  }
 
  //------------------------------------------------------------------
  override def main(argList: Array[String]) = {
    
    val start = System.currentTimeMillis
    
    //initialization
    Log = LoggerFactory.getLogger(this.getClass)
    loggerFile = new MyFile("output",innmediateWrite = true)
    info("logger starts")    
     
    //parse user input
    parseUserConfiguration(argList)
 
    //main operations
    connectWithMongoDB(uri, databaseName, collectionName )

    if (dropCollection) mongoCollection.drop  //drop current collection

    itemCountParsed = new AtomicLong(0)
    fileCountImported = new AtomicLong(0)
    
    info("Importing of GAIA CSV data file to MongoDB format starts")    
    process
    info("Elapsed time to complete all translations: " + getString(System.currentTimeMillis-start))
    info(s"Total imported sources: ${itemCountParsed.get}" )
    info(s"Total file imported:    ${fileCountImported.get}")    
    if (isParsingWolfRayet) {
      info(s"Wolf-Rayet duplicated GAIA source count: $wolfRayetDuplicatedSourceCount")
      info(s"Wolf-Rayet multiple source count: $wolfRayetMultipleSourceCount")
    }
    if (isParsingGeometricDistance)  info(s"Geometric distance import errors: $geometricDistanceError")
       
    disconnectMongoClient
    
    info("logger ends")
    System.exit(0)
  }
    
  //----------------------------------------------------------------   
}

//------------------------------------------------------------------
//------------------------------------------------------------------