//=============================================================================
//File: myFile.scala
//=============================================================================
/** It implements basic operation on a file
 *  @author  Rafael Morales Muñoz
 *  @mail    rmorales.iaa.es
 *  @version 1.0
 *  @date    9 Nov 2016
 *  @history None
 */
//=============================================================================
//=============================================================================
// Package section
//=============================================================================
package gaia

//=============================================================================
// System import section
//=============================================================================
import java.io._
import java.util.concurrent.atomic.AtomicBoolean
import java.text.SimpleDateFormat
import java.time.{Instant, ZoneId, ZonedDateTime} 
import java.time.format.DateTimeFormatter

//=============================================================================
// Class/Object implementation
//=============================================================================
//=============================================================================
object MyFile {
  
  //--------------------------------------------------------------------------
  val LineSeparator = System.getProperty("line.separator","\n")  
  val FileSeparator = java.io.File.separator // '/' in gnu-linux
  val PathYear=new SimpleDateFormat("yyyy")
  val PathYearMonth=new SimpleDateFormat("yyyy'_'MM")
  val PathYearMonthDay=new SimpleDateFormat("yyyy'_'MM'_'dd")
  
  val zoneID_UTC  =   ZoneId.of("UTC")
  val zoneID_Spain  = ZoneId.of("Europe/Madrid")
  
  //Equivalent to "DateTimeFormatter.ISO_OFFSET_DATE_TIME" format but with ms padding 
  val logTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss:SSSX").withZone( zoneID_Spain )
 
  //-------------------------------------------------------------------------  
  def getTimeStampWindowsCompatible (date: String) = {

     date.replace('-', '_')
       .replace(':', '_')
       .replace('.', '_')
  }
  //-------------------------------------------------------------------------
  def getISO_DateLocalTimeStampWindowsCompatible ( zoneID: ZoneId = zoneID_Spain ): String =  
      getTimeStampWindowsCompatible( logTimeFormatter.format( ZonedDateTime.ofInstant(Instant.now, zoneID )))   
  //--------------------------------------------------------------------------
  def ensureEndWithFileSeparator(s:String) : String =
    if (s.endsWith(FileSeparator)) s else s+FileSeparator
  //--------------------------------------------------------------------------
  def ensureDirectoryExist(s:String, printLog: Boolean = false) : Boolean = {
    val d=new java.io.File(s)
    if (!d.exists()){
      if (!d.mkdirs()){
        println(s"Error creating output directory:'$s'")
        return false
      }
      else
         if ( printLog ) println(s"Created output directory:'$d'")
    }
    if ( printLog ) println(s"Using previous output directory:'$d'")
    true
  }
  //--------------------------------------------------------------------------  
  import java.util.Date
  import java.io.{BufferedWriter,FileWriter}
  
  def createFileAndDirectoryByDate(path: String         
    ,  extension: String = ".txt"
    ,  namePrefix: String = ""
    ,  nameSuffix: String = ""
    ,  createWriter: Boolean = true) : (String, BufferedWriter)={

    //get time stamp
    val now = new Date()	   

    //create path of directories  based on date	
    ensureDirectoryExist(path)
    
    //build directory tree       
    val finalDirectory=ensureEndWithFileSeparator(path)+
      PathYear.format(now) + FileSeparator +
      PathYearMonth.format(now) + FileSeparator +
      PathYearMonthDay.format(now)+ FileSeparator
    
    //build directory list
    ensureDirectoryExist(finalDirectory)
  
    //build file name
    val fileName=finalDirectory+
    namePrefix+
    getISO_DateLocalTimeStampWindowsCompatible()+
    nameSuffix+
    extension
    
    println ( s"Using for logging: '$fileName'" )
    if (createWriter)
      ( fileName, new BufferedWriter( new FileWriter( fileName )))
     else
      ( fileName, null)
  }  
  //--------------------------------------------------------------------------
}
//=============================================================================
import MyFile._

case class MyFile(rootPath: String
  ,  innmediateWrite : Boolean = false
  ,  extension : String = ".txt"
  ,  namePrefix : String = ""
  ,  nameSuffix : String = "") {  //true data is buffered before effective write
  
  //-------------------------------------------------------------------------
  // Variable declaration
  //-------------------------------------------------------------------------
  private var name : String ="Not set"
  private var writer : BufferedWriter=null  
  private var closed = new AtomicBoolean(false)
  
  //-------------------------------------------------------------------------
  // Code starts
  //-------------------------------------------------------------------------
  init(rootPath)
  
  //-------------------------------------------------------------------------  
  private def init(s:String){
    
    synchronized{
      val result = createFileAndDirectoryByDate( rootPath, extension, namePrefix, nameSuffix )
      closed.set(false)
      name = result._1
      writer = result._2
    }
  }
  
  //-------------------------------------------------------------------------  
  def getName = name
  
  //-------------------------------------------------------------------------  
  def getAbsolutePath = new File(name).getAbsolutePath
  
  //-------------------------------------------------------------------------  
  def write(s:String) : Boolean = {
    try {
       synchronized{ 
         writer.write(s)
         if (innmediateWrite) flush
       }     
       true
    } 
    catch {
      case e: Exception => println (s"Error writting string '$s' in file '$name'. Exception:"+e.toString())
      return false
    }
  }
  
  //-------------------------------------------------------------------------  
  def writeLine(s:String) = write(s+LineSeparator)
  
  //-------------------------------------------------------------------------  
  def close : Boolean = {
          
    if(closed.get){     
      printf(s"File '$name' is already closed")
      false
    }
      
    try {
      synchronized{
        writer.flush
        writer.close
      }
      closed.set(true)
      true
    } 
    catch {
      case e: Exception => println (s"Error closing file '$name'. Exception:"+e.toString())
      closed.set(true)
      false
    }
  }

  //-------------------------------------------------------------------------  
  def reStart(initialMessage: String = "") {
    
    synchronized{
      close    
      init(rootPath)
      if(!initialMessage.isEmpty) write(initialMessage)
    }
  }
  
  //-------------------------------------------------------------------------  
  def flush = synchronized{writer.flush}
  
  //-------------------------------------------------------------------------
  //-------------------------------------------------------------------------
} //End of case class 'MyFile'

//=============================================================================
// End of 'myFile.scala' file
//=============================================================================
