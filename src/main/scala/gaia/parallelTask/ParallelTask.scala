/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  27/abr/2019
 * Time:  07h:33m
 * Description: Creates a queue of items to process and processing them in parallel
 */
//=============================================================================
package gaia.parallelTask
//=============================================================================
import java.util.concurrent.{ConcurrentLinkedQueue, Executors}
import java.util.concurrent.atomic.AtomicLong
import scala.concurrent.{Await, ExecutionContext, Future}
import scala.concurrent.duration.Duration
import scala.language.reflectiveCalls
import scala.util.{Failure, Success, Try}
//=============================================================================
//=============================================================================
//=============================================================================
//set 'isItemProcessingThreadSafe' to true when 'userProcessSingleItem' distribution.function is not thread safe (i.e. modify global/shared data)
abstract class ParallelTask [T] (itemSeq: Seq[T]
                                 , threadCount: Int
                                 , isItemProcessingThreadSafe : Boolean
                                 , randomStartMaxMsWait: Int = 1000
                                 , verbose: Boolean = true) {
  //---------------------------------------------------------------------------
  def userProcessSingleItem (t :T) : Unit
  //---------------------------------------------------------------------------
  private lazy val queue : ConcurrentLinkedQueue[T] =  new ConcurrentLinkedQueue[T]
  private val random = scala.util.Random
  private val finalThreadCount = Math.min(threadCount,if (itemSeq.isEmpty) 1 else itemSeq.length)
  private val totalItemToProcess : Long = itemSeq.length
  private val itemProcessed : AtomicLong = new AtomicLong(0)

  //---------------------------------------------------------------------------
  final val SecondsInOneMinute = 		60
  final val SecondsInOneHour = SecondsInOneMinute * 60
  final val SecondsInOneDay =  SecondsInOneHour * 24
  //-------------------------------------------------------------------------
  def getFormattedElapseTimeFromStart(startTime: Long): String = getFormattedElapseTimeFromStartInMs(System.currentTimeMillis - startTime)
  //-------------------------------------------------------------------------
  def getFormattedElapseTimeFromStartInMs(ms:Long) : String = {

    var remainSeconds= ms / 1000

    val dayCount = remainSeconds / SecondsInOneDay
    remainSeconds %= SecondsInOneDay

    val hourCount = remainSeconds / SecondsInOneHour
    remainSeconds %= SecondsInOneHour

    val minuteCount = remainSeconds / SecondsInOneMinute
    val secondsCount =  remainSeconds % SecondsInOneMinute

    "%d days %02dh %02dm %02ds".format(dayCount, hourCount, minuteCount, secondsCount)
  }
  //---------------------------------------------------------------------------
  implicit val executionContext = new ExecutionContext {
    val threadPool = Executors.newFixedThreadPool(finalThreadCount)
    override def reportFailure(cause: Throwable): Unit = {}
    override def execute(runnable: Runnable): Unit = threadPool.submit(runnable)
    def shutdown() = threadPool.shutdown()
  }
  //---------------------------------------------------------------------------
  run
  //---------------------------------------------------------------------------
  private def singleItemProcessing() {
    var item: T =  null.asInstanceOf[T]
    while(!queue.isEmpty) {
      item = queue.poll
      if(item != null) { //item processing

        Try { userProcessSingleItem( item ) }
        match {
          case Success(_) =>
          case Failure(e) => error(e.toString)
        }

        itemProcessed.addAndGet(1)
        if (verbose) println(s"Remain ${totalItemToProcess -  itemProcessed.get()} items")
      }
    }
  }
  //---------------------------------------------------------------------------
  private def parallelProcessing(): Unit = {
    val taskList: Seq[Future[Unit]] =
      for (_ <- 1 to finalThreadCount) yield {
        Thread.sleep(random.nextInt(randomStartMaxMsWait))
        Future {
          if (isItemProcessingThreadSafe) singleItemProcessing
          else synchronized (singleItemProcessing)
        }
      }

    val aggregated = Future.sequence(taskList)
    Await.result(aggregated, Duration.Inf)
  }
  //---------------------------------------------------------------------------
  private def run(): Unit ={
    //init queue
    itemSeq.foreach{ queue.add( _ ) }
    val queueSize = queue.size
    if (verbose) println(s"Processing $queueSize items using $finalThreadCount threads")

    val startTime = System.currentTimeMillis
    parallelProcessing
    if (verbose)
      println("Queue processing time: " + getFormattedElapseTimeFromStart(startTime) +
        ". Items processed: " + itemProcessed.get() +
        s". Remain ${totalItemToProcess -  itemProcessed.get()} items")
    executionContext.shutdown
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file ParallelTask.scala
//=============================================================================
